INSERT INTO PRODUCTS VALUES
('P1234', 'iPhone 6s', 'Apple iPhone 6s smartphone', '500', 'Apple', 'Smartphone', 10, 450, 0, FALSE),
('P1235', 'Dell Inspiron', 'Dell Inspiron 14-inch Laptop', 700, 'Dell', 'Laptop', 20, 1000, 0, FALSE),
('P1236', 'Nexus 7', 'Google Nexus 7', 300, 'Google', 'Tablet', 30, 1000, 0, FALSE);
----------

INSERT INTO CUSTOMERS VALUES
(1, 'David', 'New York', '3'),
(2, 'Michael', 'London', '5');