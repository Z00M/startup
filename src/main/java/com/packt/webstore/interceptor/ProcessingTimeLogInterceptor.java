package com.packt.webstore.interceptor;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.lang.String.*;
import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.currentThread;
import static java.util.Optional.ofNullable;
import static org.springframework.security.core.context.SecurityContextHolder.*;

public class ProcessingTimeLogInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = Logger.getLogger(ProcessingTimeLogInterceptor.class);

    private ThreadLocal<Long> requestStartTime = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        requestStartTime.set(currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        LOGGER.info(format("Thread[%s], user[%s], taken to process[%s ms], the request[%s]",
                currentThread().getId(),
                getContext().getAuthentication().getName(),
                currentTimeMillis() - requestStartTime.get(),
                getFullRequestPath(request)));
    }

    private String getFullRequestPath(HttpServletRequest request) {
        return request.getRequestURL() + ofNullable(request.getQueryString()).map(e -> "?" + request.getQueryString()).orElse("");
    }
}
