package com.packt.webstore.validator.impl;

import com.packt.webstore.domain.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Objects.*;

public class ProductImageValidator implements Validator {

    @Value("${product.image.size.max}")
    private long maxSize;

    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MultipartFile productImage = ((Product) target).getProductImage();

        if (isNull(productImage) || productImage.isEmpty()) {
            errors.rejectValue("productImage", "validator.productImage.message.empty");
            return;
        }

        if (productImage.getSize() > maxSize) {
            errors.rejectValue("productImage", "validator.productImage.message.size", new Object[]{maxSize, productImage.getSize()}, "");
        }
    }
}
