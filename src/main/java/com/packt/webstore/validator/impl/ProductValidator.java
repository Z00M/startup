package com.packt.webstore.validator.impl;

import com.packt.webstore.domain.Product;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.Set;

@RequiredArgsConstructor
public class ProductValidator implements Validator{

    @Autowired
    private LocalValidatorFactoryBean localValidatorFactoryBean;

    @NonNull
    private final Set<Validator> springValidators;

    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        localValidatorFactoryBean.validate(target).forEach(violation -> errors.rejectValue(violation.getPropertyPath().toString(), "", violation.getMessage()));

        springValidators.forEach(validator -> validator.validate(target, errors));
    }
}
