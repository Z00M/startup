package com.packt.webstore.validator.impl;

import com.google.common.collect.ImmutableSet;
import com.packt.webstore.validator.annotation.Category;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

public class CategoryValidator implements ConstraintValidator<Category, String> {

    private Set<String> productCategories = ImmutableSet.of("Smartphone", "Laptop", "Tablet");

    @Override
    public void initialize(Category constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return productCategories.contains(value);
    }
}
