package com.packt.webstore.validator.impl;

import com.packt.webstore.domain.Product;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

import static java.util.Objects.isNull;

public class UnitsInStockValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Product product = (Product) target;

        if (isNull(product.getUnitPrice()) ||
                (new BigDecimal(1000).compareTo(product.getUnitPrice()) <= 0 && product.getUnitsInStock() > 99)) {
            errors.rejectValue("unitsInStock", "validator.UnitsInStockValidator.message");
        }
    }
}
