package com.packt.webstore.validator.annotation;

import com.packt.webstore.validator.impl.ProductIdValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ProductIdValidator.class)
public @interface ProductId {
    String message() default "{validator.annotation.ProductId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
