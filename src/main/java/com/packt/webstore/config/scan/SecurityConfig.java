package com.packt.webstore.config.scan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("john")
                                     .password("pa55word")
                                     .roles("USER");

        auth.inMemoryAuthentication().withUser("admin")
                                     .password("root123")
                                     .roles("USER","ADMIN");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .formLogin().loginPage("/login")
                .defaultSuccessUrl("/market/products/add")
                .failureUrl("/login?error")
                .usernameParameter("userId")
                .passwordParameter("password")
                .and()
                .rememberMe().key("remember-me").tokenValiditySeconds(60 * 60 * 24);

        httpSecurity
                .logout().logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .addLogoutHandler(new SecurityContextLogoutHandler());

        httpSecurity.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/**/add").access("hasRole('ADMIN')");
//                .antMatchers("/**/market/**").access("hasRole('USER')");

        httpSecurity.exceptionHandling().accessDeniedPage("/login?accessDenied");

        //todo: add expire session url and page instead of spring stub
        httpSecurity.sessionManagement().maximumSessions(1);

        //todo: change binding cart to user form session id
        httpSecurity.sessionManagement().sessionFixation().none();

        httpSecurity.csrf().disable();

//        uncomment to enable https fro app.
//        Note: in tomcat server.xml should be defined secured connector
//        httpSecurity.requiresChannel()
//                .antMatchers("/*").requiresSecure();
    }
}
