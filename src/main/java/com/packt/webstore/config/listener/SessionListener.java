package com.packt.webstore.config.listener;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

public class SessionListener implements HttpSessionListener {

    private static final Logger LOGGER = Logger.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        event.getSession().setMaxInactiveInterval(60 * 5);
        LOGGER.info(String.format("created session[%s], for user[%s]",
                event.getSession().getId(),
                getContext().getAuthentication().getName()));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        LOGGER.info(String.format("destroyed session[%s], for user[%s]",
                event.getSession().getId(),
                getContext().getAuthentication().getName()));
    }
}
