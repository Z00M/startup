package com.packt.webstore.controller.scan;

import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class SubmitController {

    @RequestMapping(value = "/submit", method = POST)
    public void submit(@RequestBody Input input) {
        System.out.println(input);
    }

    @RequestMapping(value = "/submit1", method = POST)
    public void submit1(@RequestParam String username, @RequestParam String usersurname) {
        System.out.println(username);
        System.out.println(usersurname);
    }

    @Setter
    @ToString
    private static class Input {
        private String name;
        private String surname;
    }
}
