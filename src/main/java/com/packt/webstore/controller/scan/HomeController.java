package com.packt.webstore.controller.scan;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {
    @RequestMapping("/welcome")
    public String welcome(Model model) {
        if (!model.containsAttribute("greeting")) {
            model.addAttribute("greeting", "Welcome to Web Store!");
        }
        if (!model.containsAttribute("tagline")) {
            model.addAttribute("tagline", "The one and only amazing web store");
        }

        return "welcome";
    }

    @RequestMapping("/welcome/greeting")
    public String greeting(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("greeting", "Welcome to Web Store! - redirected from greeting");
        redirectAttributes.addFlashAttribute("tagline", "The one and only amazing web store! - redirected from greeting");

        return "redirect:/welcome";
    }
}
