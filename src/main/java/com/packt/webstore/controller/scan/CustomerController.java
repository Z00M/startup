package com.packt.webstore.controller.scan;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customers")
    public String list(Model model) {
        model.addAttribute("customers", customerService.getAllCustomers());
        return "customers";
    }

    @RequestMapping("/customers/add")
    public String getAddNewCustomerForm(@ModelAttribute("newCustomer") Customer newCustomer) {
        return "addCustomer";
    }

    @RequestMapping(value = "/customers/add", method = POST)
    public String addCustomer(@ModelAttribute Customer newCustomer) {
        customerService.addCustomer(newCustomer);
        return "redirect:/customers";
    }
}
