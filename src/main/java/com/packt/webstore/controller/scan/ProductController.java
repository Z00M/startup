package com.packt.webstore.controller.scan;

import com.google.common.collect.ImmutableMap;
import com.packt.webstore.controller.exception.NoProductsFoundUnderCategoryException;
import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.ProductCondition;
import com.packt.webstore.domain.repository.exception.ProductNotFoundException;
import com.packt.webstore.service.ProductService;
import com.packt.webstore.validator.impl.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.util.List;
import java.util.Map;

import static java.util.Objects.*;
import static org.springframework.util.StringUtils.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("market")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductValidator productValidator;

    @RequestMapping("/products")
    public String list(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "products";
    }

    @RequestMapping("/update/stock")
    public String updateStock() {
        productService.updateAllStock();
        return "redirect:/market/products";
    }

    @RequestMapping("/products/{category}")
    public String getProductsByCategory(Model model, @PathVariable String category) {
        List<Product> productsByCategory = productService.getProductsByCategory(category);

        //todo: add exception handling
        if (productsByCategory.isEmpty()) {
            throw new NoProductsFoundUnderCategoryException();
        }

        model.addAttribute("products", productsByCategory);
        return "products";
    }

    @RequestMapping("/products/filter/{filterParams}")
    public String getProductsByFilter(@MatrixVariable Map<String, List<String>> filterParams, Model model) {
        model.addAttribute("products", productService.getProductsByFilter(filterParams));
        return "products";
    }

    @RequestMapping("/product")
    public String getProductById(@RequestParam("id") String productId, Model model) {
        model.addAttribute("product", productService.getProductById(productId));
        return "product";
    }

    @RequestMapping("/products/{category}/{price}")
    public String getProductsDifferentFilters(@PathVariable String category,
                                              @MatrixVariable Map<String, String> price,
                                              @RequestParam String brand,
                                              Model model) {
        Integer lowPrice = Integer.valueOf(price.get("low"));
        Integer highPrice = Integer.valueOf(price.get("high"));

        model.addAttribute("products", productService.getProductsByFilter(category, ImmutableMap.of("low", lowPrice, "high", highPrice), brand));
        return "products";
    }

    @RequestMapping("/products/add")
    public String getAddNewProductForm(@ModelAttribute("newProduct") Product newProduct, Model model) {
        model.addAttribute("productConditionEnum", ProductCondition.values());
        return "addProduct";
    }

    @RequestMapping(value = "/products/add", method = POST)
    public String processAddNewProductForm(@ModelAttribute("newProduct") @Valid Product productToBeAdded,
                                           BindingResult result,
                                           HttpServletRequest request,
                                           Model model) {
        if (result.getSuppressedFields().length > 0) {
            throw new RuntimeException("Attempting to bind disallowed fields: " + arrayToCommaDelimitedString(result.getSuppressedFields()));
        }

        if (result.hasErrors()) {
            model.addAttribute("productConditionEnum", ProductCondition.values());
            return "addProduct";
        }

        MultipartFile productImage = productToBeAdded.getProductImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        if (nonNull(productImage) && !productImage.isEmpty()) {
            try {
                productImage.transferTo(new File(rootDirectory+"resources\\images\\" + productToBeAdded.getProductId() + ".png"));
            } catch (Exception e) {
                throw new RuntimeException("Product Image saving failed", e);
            }
        }

        productService.addProduct(productToBeAdded);
        return "redirect:/market/products";
    }

    @RequestMapping("/products/invalidPromoCode")
    public String invalidPromoCode() {
        return "invalidPromoCode";
    }

    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setValidator(productValidator);
        binder.setAllowedFields("productId",
                "name",
                "unitPrice",
                "description",
                "manufacturer",
                "category",
                "unitsInStock",
                "condition",
                "productImage");
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ModelAndView handleError(HttpServletRequest request, ProductNotFoundException e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("invalidProductId", e.getProductId());
        modelAndView.addObject("exception", e);
        modelAndView.addObject("url", request.getRequestURL() + "?" + request.getQueryString());
        modelAndView.setViewName("productNotFound");
        return modelAndView;
    }
}
