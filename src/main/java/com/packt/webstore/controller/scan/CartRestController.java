package com.packt.webstore.controller.scan;

import com.packt.webstore.domain.Cart;
import com.packt.webstore.dto.CartDto;
import com.packt.webstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "rest/cart")
public class CartRestController {

    @Autowired
    private CartService cartService;

    @RequestMapping(method = POST)
    @ResponseStatus(value = CREATED)
    public void create(@RequestBody CartDto cartDto) {
        cartService.create(cartDto);
    }

    @RequestMapping(value = "/{cartId}")
    public Cart read(@PathVariable String cartId) {
        return cartService.read(cartId);
    }

    @RequestMapping(value = "/{cartId}", method = PUT)
    public void update(@PathVariable String cartId, @RequestBody CartDto cartDto) {
        cartDto.setId(cartId);
        cartService.update(cartId, cartDto);
    }

    @RequestMapping(value = "/{cartId}", method = DELETE)
    public void delete(@PathVariable String cartId) {
        cartService.delete(cartId);
    }

    @RequestMapping(value = "/add/{productId}", method = PUT)
    public void addItem(@PathVariable String productId, HttpSession session) {
        cartService.addItem(session.getId(), productId);
    }

    @RequestMapping(value = "/remove/{productId}", method = PUT)
    public void removeItem(@PathVariable String productId, HttpSession session) {
        cartService.removeItem(session.getId(), productId);
    }
}
