package com.packt.webstore.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CartItemDto implements Serializable {
    private static final long serialVersionUID = - 3551573319376880896L;

    private String id;
    private String productId;
    private int quantity;
}
