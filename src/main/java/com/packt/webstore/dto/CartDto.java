package com.packt.webstore.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CartDto implements Serializable {
    private static final long serialVersionUID = -2017182726290898588L;

    private String id;
    private List<CartItemDto> cartItems;

    public CartDto(String id) {
        this.id = id;
        cartItems = new ArrayList<>();
    }

    public void addCartItem(CartItemDto cartItemDto) {
        cartItems.add(cartItemDto);
    }
}
