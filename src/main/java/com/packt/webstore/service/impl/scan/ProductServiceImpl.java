package com.packt.webstore.service.impl.scan;

import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;
import com.packt.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void updateAllStock() {
        List<Product> allProducts = productRepository.getAllProducts();
        allProducts.stream()
                .filter(product -> product.getUnitsInStock() < 500)
                .forEach(product -> productRepository.updateStock(product.getProductId(), product.getUnitsInStock() + 1000));
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public List<Product> getProductsByCategory(String category) {
        return productRepository.getProductsByCategory(category);
    }

    @Override
    public List<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
        return productRepository.getProductsByFilter(filterParams);
    }

    @Override
    public Product getProductById(String productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public List<Product> getProductsByFilter(String category, Map<String, Integer> priceFilter, String brand) {
        return productRepository.getProductsByFilter(category, priceFilter, brand);
    }

    @Override
    @Secured("ADMIN")
    public void addProduct(Product product) {
        productRepository.addProduct(product);
    }
}
