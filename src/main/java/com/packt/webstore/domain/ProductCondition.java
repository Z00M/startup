package com.packt.webstore.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductCondition {
    NEW(10),
    OLD(20),
    REFUBISHED(30);

    private int id;

    public static ProductCondition getProductConditionById(int id) {
        for (ProductCondition elem : ProductCondition.values()) {
            if (elem.id == id) {
                return elem;
            }
        }
        throw new IllegalArgumentException("illegal value of state");
    }
}
