package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(of = "customerId")
public class Customer1 implements Serializable {

    private static final long serialVersionUID = -3814717561692307390L;

    private Long customerId;
    private String name;
    private Address billingAddress;
    private String phoneNumber;

    public Customer1() {
        billingAddress = new Address();
    }
}
