package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(of = "orderId")
public class Order implements Serializable{

    private Long orderId;
    private Cart cart;
    private Customer1 customer1;
    private ShippingDetail shippingDetail;

    public Order() {
        customer1 = new Customer1();
        shippingDetail = new ShippingDetail();
    }
}
