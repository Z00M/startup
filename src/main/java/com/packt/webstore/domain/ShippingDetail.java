package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ShippingDetail implements Serializable {

    private static final long serialVersionUID = 7786839908124907474L;

    private Long id;
    private String name;
//    private LocalDate shippingDate;
    private String shippingDate;
    private Address shippingAddress;

    public ShippingDetail() {
        shippingAddress = new Address();
    }
}
