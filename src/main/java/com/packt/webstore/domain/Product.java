package com.packt.webstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.packt.webstore.validator.annotation.Category;
import com.packt.webstore.validator.annotation.ProductId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "productId")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product implements Serializable {
    private static final long serialVersionUID = 3678107792576131001L;

    @Pattern(regexp = "P[1-9]+", message = "{Pattern.Product.productId.validation}")
    @ProductId
    private String productId;

    @Size(min = 4, max = 50, message = "{Size.Product.name.validation}")
    private String name;

    @Min(value = 0, message = "{Min.Product.unitPrice.validation}")
    @Digits(integer = 8, fraction = 2, message = "{Digits.Product.unitPrice.validation}")
    @NotNull(message = "{NotNull.Product.unitPrice.validation}")
    private BigDecimal unitPrice;

    @NotBlank
    private String description;

    @NotBlank
    private String manufacturer;

    @Category
    private String category;

    @Min(value = 0, message = "{Min.Product.unitPrice.validation}")
    @Digits(integer = 8, fraction = 0, message = "{Digits.Product.unitPrice.validation}")
    private long unitsInStock;

    @Min(value = 0, message = "{Min.Product.unitPrice.validation}")
    @Digits(integer = 8, fraction = 0, message = "{Digits.Product.unitPrice.validation}")
    private long unitsInOrder;

    private boolean discontinued;

    @NotNull
    private ProductCondition condition;

    @XmlTransient
    @JsonIgnore
    private MultipartFile productImage;

    public Product(String productId, String name, BigDecimal unitPrice) {
        this.productId = productId;
        this.name = name;
        this.unitPrice = unitPrice;
    }
}
