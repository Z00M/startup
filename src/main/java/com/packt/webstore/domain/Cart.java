package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import static java.util.Objects.nonNull;

@RequiredArgsConstructor
@EqualsAndHashCode(of = "id")
public class Cart implements Serializable {
    private static final long serialVersionUID = 6554623865768217431L;

    @Getter
    private final String id;

    @Getter
    @Setter
    private List<CartItem> cartItems;

    private BigDecimal grandTotal;

    public BigDecimal getGrandTotal() {
        grandTotal = cartItems.stream()
                .map(CartItem::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return grandTotal;
    }

    public CartItem getItemByProductId(String productId) {
        return cartItems.stream()
                .filter(cartItem -> cartItem.getProduct().getProductId().equals(productId))
                .findAny()
                .orElse(null);
    }

    public boolean containsProduct(String productId) {
        return nonNull(getItemByProductId(productId));
    }
}
