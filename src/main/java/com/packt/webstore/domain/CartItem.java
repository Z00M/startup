package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@EqualsAndHashCode(of = "id")
@RequiredArgsConstructor
public class CartItem implements Serializable {
    private static final long serialVersionUID = -4546941350577482213L;

    @Getter
    private final String id;

    @Getter
    private Product product;

    @Getter
    @Setter
    private int quantity;

    private BigDecimal totalPrice;

    public void setProduct(Product product) {
        this.product = product;
        quantity = 1;
        updateTotalPrice();
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    private void updateTotalPrice() {
        totalPrice = product.getUnitPrice().multiply(new BigDecimal(quantity));
    }
}
