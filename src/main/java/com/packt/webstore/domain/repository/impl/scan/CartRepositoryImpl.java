package com.packt.webstore.domain.repository.impl.scan;

import com.google.common.collect.ImmutableMap;
import com.packt.webstore.domain.Cart;
import com.packt.webstore.domain.CartItem;
import com.packt.webstore.domain.repository.CartRepository;
import com.packt.webstore.domain.repository.impl.mapper.CartMapper;
import com.packt.webstore.dto.CartDto;
import com.packt.webstore.dto.CartItemDto;
import com.packt.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;

@Repository
public class CartRepositoryImpl implements CartRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTempleate;
    @Autowired
    private ProductService productService;

    @Override
    public void create(CartDto cartDto) {
        final String INSERT_CART_SQL = "INSERT INTO CART(ID) VALUES (:id)";
        jdbcTempleate.update(INSERT_CART_SQL, ImmutableMap.of("id", cartDto.getId()));

        cartDto.getCartItems().forEach(cartItemDto -> {
            String INSERT_CART_ITEM_SQL = "INSERT INTO CART_ITEM(ID,PRODUCT_ID ,CART_ID, QUANTITY) VALUES (:id, :product_id, :cart_id, :quantity)";
            Map<String, Object> cartItemsParams = ImmutableMap.<String, Object>builder()
                    .put("id", cartItemDto.getId())
                    .put("product_id", cartItemDto.getProductId())
                    .put("cart_id", cartDto.getId())
                    .put("quantity", cartItemDto.getQuantity()).build();

            jdbcTempleate.update(INSERT_CART_ITEM_SQL, cartItemsParams);
        });
    }

    @Override
    public Cart read(String id) {
        String SQL = "SELECT * FROM CART WHERE ID = :id";
        CartMapper cartMapper = new CartMapper(jdbcTempleate, productService);
        try {
            return jdbcTempleate.queryForObject(SQL, singletonMap("id", id), cartMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void update(String id, CartDto cartDto) {
        List<CartItemDto> cartItems = cartDto.getCartItems();
        for (CartItemDto cartItemDto : cartItems) {
            String SQL = "UPDATE CART_ITEM SET QUANTITY = :quantity, PRODUCT_ID = :product_id WHERE ID = :id AND CART_ID = :cart_id ";
            Map<String, Object> params = ImmutableMap.<String, Object>builder()
                    .put("id", cartItemDto.getId())
                    .put("product_id", cartItemDto.getProductId())
                    .put("cart_id", cartDto.getId())
                    .put("quantity", cartItemDto.getQuantity()).build();
            jdbcTempleate.update(SQL, params);
        }
    }

    @Override
    public void delete(String id) {
        String SQL_DELETE_CART_ITEM = "DELETE FROM CART_ITEM WHERE CART_ID = :id";
        String SQL_DELETE_CART = "DELETE FROM CART WHERE ID = :id";
        Map<String, Object> params = singletonMap("id", id);

        jdbcTempleate.update(SQL_DELETE_CART_ITEM, params);
        jdbcTempleate.update(SQL_DELETE_CART, params);

    }

    @Override
    public void addItem(String cartId, String productId) {
        Cart cart = read(cartId);
        if (cart == null) {
            CartItemDto newCartItemDto = new CartItemDto();
            newCartItemDto.setId(cartId + productId);
            newCartItemDto.setProductId(productId);
            newCartItemDto.setQuantity(1);
            CartDto newCartDto = new CartDto(cartId);
            newCartDto.addCartItem(newCartItemDto);
            create(newCartDto);
            return;
        }

        Map<String, Object> cartItemsParams = new HashMap<>();
        String SQL;
        if (!cart.containsProduct(productId)) {
            SQL = "INSERT INTO CART_ITEM (ID, PRODUCT_ID, CART_ID, QUANTITY) VALUES (:id, :productId, :cartId, :quantity)";
            cartItemsParams.put("id", productId);
            cartItemsParams.put("quantity", 1);
        } else {
            CartItem existingItem = cart.getItemByProductId(productId);
            SQL = "UPDATE CART_ITEM SET QUANTITY = :quantity WHERE CART_ID = :cartId AND PRODUCT_ID = :productId ";
            cartItemsParams.put("id", existingItem.getId());
            cartItemsParams.put("quantity", existingItem.getQuantity() + 1);
        }
        cartItemsParams.put("productId", productId);
        cartItemsParams.put("cartId", cartId);
        jdbcTempleate.update(SQL, cartItemsParams);
    }

    @Override
    public void removeItem(String cartId, String productId) {
        String SQL_DELETE_CART_ITEM = "DELETE FROM CART_ITEM WHERE PRODUCT_ID = :productId AND CART_ID = :id";
        jdbcTempleate.update(SQL_DELETE_CART_ITEM, ImmutableMap.of("id", cartId, "productId", productId));
    }

    @Override
    public void clearCart(String cartId) {
        String SQL_DELETE_CART_ITEM = "DELETE FROM CART_ITEM WHERE CART_ID = :id";
        jdbcTempleate.update(SQL_DELETE_CART_ITEM, singletonMap("id", cartId));
    }
}
