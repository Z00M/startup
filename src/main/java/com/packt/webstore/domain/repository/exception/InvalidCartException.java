package com.packt.webstore.domain.repository.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class InvalidCartException extends RuntimeException {

    private static final long serialVersionUID = -2476457185676640203L;

    private final String cardId;
}

