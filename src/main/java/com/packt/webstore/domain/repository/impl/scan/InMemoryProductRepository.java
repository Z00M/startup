package com.packt.webstore.domain.repository.impl.scan;

import com.google.common.collect.ImmutableMap;
import com.packt.webstore.domain.repository.exception.ProductNotFoundException;
import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.packt.webstore.domain.ProductCondition.*;
import static java.util.Collections.emptyMap;

@Repository
public class InMemoryProductRepository implements ProductRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<Product> getAllProducts() {
        return jdbcTemplate.query("SELECT * FROM products", emptyMap(), new ProductMapper());
    }

    @Override
    public void updateStock(String productId, long noOfUnits) {
        String sql = "UPDATE PRODUCTS SET UNITS_IN_STOCK = :unitsInStock WHERE ID = :id";
        jdbcTemplate.update(sql, ImmutableMap.of("unitsInStock", noOfUnits, "id", productId));
    }

    @Override
    public List<Product> getProductsByCategory(String category) {
        String sql = "SELECT * FROM products WHERE CATEGORY = :category";
        return jdbcTemplate.query(sql, ImmutableMap.of("category", category), new ProductMapper());
    }

    @Override
    public List<Product> getProductsByFilter(Map<String, List<String>> filterParams) {
        String sql = "SELECT * FROM PRODUCTS WHERE CATEGORY IN (:categories) AND MANUFACTURER IN (:brands)";
        return jdbcTemplate.query(sql, filterParams, new ProductMapper());
    }

    @Override
    public Product getProductById(String productId) {
        String sql = "SELECT * FROM PRODUCTS WHERE ID = :id";
        try {
            return jdbcTemplate.queryForObject(sql, ImmutableMap.of("id", productId), new ProductMapper());
        } catch (DataAccessException e) {
            throw new ProductNotFoundException(productId);
        }
    }

    @Override
    public List<Product> getProductsByFilter(String category, Map<String, Integer> priceFilter, String brand) {
        String sql = "SELECT * FROM products WHERE CATEGORY = :category AND MANUFACTURER = :brand and UNIT_PRICE BETWEEN :low and :high";
        Map<String, Object> params = new HashMap<>();
        params.put("category", category);
        params.put("brand", brand);
        params.put("low", priceFilter.get("low"));
        params.put("high", priceFilter.get("high"));

        return jdbcTemplate.query(sql, params, new ProductMapper());
    }

    @Override
    public void addProduct(Product product) {
        String sql = "insert into products (id," +
                "name," +
                "description," +
                "unit_price," +
                "manufacturer," +
                "category," +
                "condition," +
                "units_in_stock," +
                "units_in_order," +
                "discontinued)" +
                "values(:id, :name, :desc, :price, :manufacturer, :category, :condition, :inStock, :inOrder, :discontinued)";

        Map<String, Object> params = new HashMap<>();
        params.put("id", product.getProductId());
        params.put("name", product.getName());
        params.put("desc", product.getDescription());
        params.put("price", product.getUnitPrice());
        params.put("manufacturer", product.getManufacturer());
        params.put("category", product.getCategory());
        params.put("condition", product.getCondition().getId());
        params.put("inStock", product.getUnitsInStock());
        params.put("inOrder", product.getUnitsInOrder());
        params.put("discontinued", product.isDiscontinued());

        jdbcTemplate.update(sql, params);
    }

    private static final class ProductMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            Product product = new Product();
            product.setProductId(rs.getString("ID"));
            product.setName(rs.getString("NAME"));
            product.setDescription(rs.getString("DESCRIPTION"));
            product.setUnitPrice(rs.getBigDecimal("UNIT_PRICE"));
            product.setManufacturer(rs.getString("MANUFACTURER"));
            product.setCategory(rs.getString("CATEGORY"));
            product.setCondition(getProductConditionById(rs.getInt("CONDITION")));
            product.setUnitsInStock(rs.getLong("UNITS_IN_STOCK"));
            product.setUnitsInOrder(rs.getLong("UNITS_IN_ORDER"));
            product.setDiscontinued(rs.getBoolean("DISCONTINUED"));
            return product;
        }
    }
}
