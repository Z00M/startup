package com.packt.webstore.domain.repository.impl.scan;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyMap;

@Repository
public class InMemoryCustomerRepository implements CustomerRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<Customer> getAllCustomers() {
        return jdbcTemplate.query("SELECT * FROM customers", emptyMap(), new CustomerMapper());
    }

    @Override
    public void addCustomer(Customer newCustomer) {
        String sql = "insert into customers (id," +
                "name," +
                "address," +
                "no_of_orders_made)" +
                "values(:id, :name, :address, :no_of_orders_made)";

        Map<String, Object> params = new HashMap<>();
        params.put("id", newCustomer.getCustomerId());
        params.put("name", newCustomer.getName());
        params.put("address", newCustomer.getAddress());
        params.put("no_of_orders_made", newCustomer.getNoOfOrdersMade());

        jdbcTemplate.update(sql, params);
    }

    private static final class CustomerMapper implements RowMapper<Customer> {
        @Override
        public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
            Customer customer = new Customer();
            customer.setCustomerId(rs.getInt("ID"));
            customer.setName(rs.getString("NAME"));
            customer.setAddress(rs.getString("ADDRESS"));
            customer.setNoOfOrdersMade(rs.getInt("NO_OF_ORDERS_MADE"));
            return customer;
        }
    }
}
