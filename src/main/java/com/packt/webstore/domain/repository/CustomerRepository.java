package com.packt.webstore.domain.repository;

import com.packt.webstore.domain.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository {
    List<Customer> getAllCustomers();
    void addCustomer(Customer newCustomer);
}
