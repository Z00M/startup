package com.packt.webstore.domain.repository.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 694354952032299587L;

    private String productId;
}
