package com.packt.webstore.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = "customerId")
public class Customer {
    private int customerId;
    private String name;
    private String address;
    private long noOfOrdersMade;
}
