package com.packt.webstore.webservice;

import com.packt.webstore.domain.Product;

import java.util.List;

public interface ProductWs {
    List<Product> getAllProducts();
}
