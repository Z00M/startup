window.sendData = function () {
    var xhr = new XMLHttpRequest();

    var json = JSON.stringify({
        name: document.forms.myform.username.value,
        surname: document.forms.myform.surname.value
    });

    xhr.open("POST", 'submit', true);
    xhr.setRequestHeader('Content-type', 'application/json;charset=utf-8');

    xhr.send(json);
}
