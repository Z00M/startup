package com.packt.webstore.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CartItemTest {

    private CartItem cartItem;

    @Before
    public void setUp() {
        cartItem = new CartItem("1");
    }

    @Test
    public void test_cartItem_total_price_should_be_equal_to_product_unit_price_in_case_of_single_quantity() {
        Product iphone = new Product("P1234", "iPhone 5s", new BigDecimal(500));
        cartItem.setProduct(iphone);

        BigDecimal totalPrice = cartItem.getTotalPrice();

        assertThat(iphone.getUnitPrice(), equalTo(totalPrice));
    }
}
