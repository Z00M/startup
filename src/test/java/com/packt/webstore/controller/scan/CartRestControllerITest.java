package com.packt.webstore.controller.scan;

import com.packt.webstore.config.WebApplicationContextConfig;
import com.packt.webstore.testutil.TestDataSourceContextConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WebApplicationContextConfig.class, TestDataSourceContextConfig.class})
public class CartRestControllerITest {

    @Autowired
    private MockHttpSession session;
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(wac).build();
    }

    @Test
    @WithMockUser
    public void read_method_should_return_correct_cart_Json_object() throws Exception {
        mockMvc.perform(put("/rest/cart/add/P1234")
                .session(session))
                .andExpect(status().isOk());

        mockMvc.perform(get("/rest/cart/" + session.getId()).session(session))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cartItems[0].product.productId").value("P1234"));
    }
}
