package com.packt.webstore.validator;

import com.packt.webstore.config.WebApplicationContextConfig;
import com.packt.webstore.domain.Product;
import com.packt.webstore.testutil.TestDataSourceContextConfig;
import com.packt.webstore.validator.impl.ProductValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BindException;

import java.math.BigDecimal;

import static com.packt.webstore.domain.ProductCondition.NEW;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.validation.ValidationUtils.invokeValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WebApplicationContextConfig.class, TestDataSourceContextConfig.class})
public class ProductValidatorITest {

    @Autowired
    private ProductValidator productValidator;

    @Test
    public void test_product_without_UnitPrice_should_be_invalid() {
        Product product = new Product("productId", "productName", null);
        BindException bindException = new BindException(product, "product");

        invokeValidator(productValidator, product, bindException);

        assertThat(bindException.getFieldError("unitPrice").getDefaultMessage(), equalTo("Unit price is Invalid. It cannot be empty."));
    }

    @Test
    public void test_product_with_existing_productId_invalid() {
        Product product = new Product("P1234", "iPhone 5s", new BigDecimal(500));
        BindException bindException = new BindException(product, "product");

        invokeValidator(productValidator, product, bindException);

        assertThat(bindException.getFieldError("productId").getDefaultMessage(), equalTo("A product already exists with this product id"));
    }

    @Test
    public void test_valid_product_should_not_get_any_error_during_validation() {
        Product product = new Product("P9876", "iPhone 5s", new BigDecimal(500));
        product.setManufacturer("manufacturer");
        product.setCategory("Smartphone");
        product.setDescription("description");
        product.setCondition(NEW);
        product.setProductImage(new MockMultipartFile("name", "content".getBytes()));
        BindException bindException = new BindException(product, "product");

        invokeValidator(productValidator, product, bindException);

        assertThat(bindException.getErrorCount(), equalTo(0));
    }
}

